const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const server = http.createServer(app);
const cors = require('cors');
const path = require('path');
const SETTINGS = require('./settings');


const { DB_PATH, PORT, HOST, UPLOADS } = SETTINGS;

const DB = require('./application/modules/DB/DB');
const UsersManager = require('./application/modules/users/UsersManager');
const TasksManager = require('./application/modules/tasks/TasksManager');
const Router = require('./application/routes/Router');

const db = new DB(DB_PATH);

const usersManager = new UsersManager({ db });
const tasksManager = new TasksManager({ db });

const router = new Router({ usersManager, tasksManager });

app.use(
    bodyParser.urlencoded({ extended: true}),
    express.static(__dirname + '/public'),
);
app.use(`/${UPLOADS.FOLDER_PATH}`, express.static('uploads')); 

app.use(bodyParser.json());
app.use(cors());

app.use('/', router);


server.listen(PORT, () => console.log(`Server running at port ${PORT}. ${HOST}`));