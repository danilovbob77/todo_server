const express = require('express');
const router = express.Router();
const Answer = require('./Answer');
const multerTaskPircture = require('../modules/multer/multerTaskPicture');

/**
 * 
 * @param {*} Object 
 * @returns {Router} 
 */
function Router({ usersManager, tasksManager }) {
    const answer = new Answer();

    // *******************************
    // Авторизация, регистрация
    // *******************************
    router.post('/auth/registration', async (req, res) => {
        try {
            const value = await usersManager.registration(req.body);
            res.send(answer.good(value));   
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    // авторизация
    router.post('/auth/login', async (req, res) => {
        try {
            const value = await usersManager.login(req.body);
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    // выход
    router.get('/auth/logout/:token', async (req, res) => {
        try {
            const value = await usersManager.logout(req.params);
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    })

    // *******************************
    // Работа с данными пользователей.
    // *******************************

    router.get('/users/get_user_data/:token', async (req, res) => {
        try {
            const value = await users.getUserData(req.params);
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/users/get_all_users', async (req, res) => {
        try {
            const value = await usersManager.getAllUsers();
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });


    // *******************************
    // Работа с задачами.
    // *******************************
    router.post('/tasks/add_new_task', async (req, res) => {
        try {
            const value = await tasksManager.addNewTask(req.body);
            res.json(answer.good(value));  
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.post('/tasks/update_task', async (req, res) => {
        try {
            const value = await tasksManager.updateTask(req.body);
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.post('/tasks/update_task_status', async (req, res) => {
        try {
            const value = await tasksManager.updateTaskStatus(req.body);
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.error(900));
        }
    });

    router.post('/tasks/update_task_priority', async (req, res) => {
        try {
            const value = await tasksManager.updateTaskPriority(req.body);
            res.json(answer.good(value));  
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/remove_task', async (req, res) => {
        try {
            const value = await tasksManager.removeTask(req.query);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/get_all_tasks', async (req, res) => {
        try {
            const value = await tasksManager.getAllTasks();
            res.json(answer.good(value));
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/get_tasks_by_user/:token', async (req, res) => {
        try {
            const value = await tasksManager.getTasksByUser(req.params);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    // Работа с файлами задач.
    router.post('/tasks/save_image', multerTaskPircture, async (req, res) => {
        try {
            const value = await tasksManager.saveTaskImage({ task_image: req.file, ...req.body });
            res.json(answer.good(value));    
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/remove_image', async (req, res) => {
        try {
            const value = await tasksManager.removeTaskFile(req.query);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/get_deleted_tasks/:token', async (req, res) => {
        try {
            const value = await tasksManager.getDeletedTasksByUser(req.params);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/set_task_deleted', async (req, res) => {
        try {
            const value = await tasksManager.setTaskIsDeleted(req.query);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });

    router.get('/tasks/set_task_not_deleted', async (req, res) => {
        try {
            const value = await tasksManager.setTaskIsNotDeleted(req.query);
            res.json(answer.good(value)); 
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            res.json(answer.bad(900));
        }
    });
    

    router.all('/*', (req, res) => res.send(answer.bad(404)));
    return router;
}

module.exports = Router;