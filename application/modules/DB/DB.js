const sqlite3 = require('sqlite3').verbose();
const { open } = require('sqlite');

class DB {
    constructor(dbFilePath = "") {
        // connect to database
        (async () => {
            // open database
            this.db = await open({
                filename: dbFilePath,
                driver: sqlite3.Database
            })
        })();
    }

    /**
     * Получить запись таблицы users по колонке email.
     * 
     * @param {string} email 
     * @returns {Promise} 
     */
    getUserByEmail(email) {
        const user = this.db.get('SELECT * FROM users WHERE email=?', [email]);
        return user;
    }

    /**
     * Получить запись таблицы users по колонке name.
     * 
     * @param {string} name 
     * @returns {Promise} 
     */
     getUserByName(name) {
        const user = this.db.get('SELECT * FROM users WHERE name=?', [name]);
        return user;
    }

    /**
     * Получить все записи таблицы users.
     * @returns {Promise}
     */
    getAllUsers() {
        const users = this.db.all('SELECT * FROM users');
        return users;   
    }

    /**
     * Добавлить запись в таблицу users.
     * 
     * @param {string} name 
     * @param {string} email 
     * @param {string} password 
     * @param {string} token 
     * @returns {Promise}
     */
    addNewUser(name, email, password, token) {
        const result = this.db.run(
            'INSERT INTO users (name, email, password, token) VALUES (?, ?, ?, ?)',
            [name, email, password, token]
        );
        return result;
    }

    /**
     * Обновить колонку token в таблице users.
     * 
     * @param {number} id 
     * @param {id} token 
     * @returns {Promise}
     */
    updateUserToken(id, token) {
        const result = this.db.run(
            'UPDATE users SET token=? WHERE id=?',
            [token, id]
        );
        return result;
    }

    /**
     * Очистить колонку token в таблице users в записи найденной по token.
     * 
     * @param {string} token 
     * @returns {Promise}
     */
    deleteUserToken(token) {
        return this.db.run(
            "UPDATE users SET token='' WHERE token=?",
            [token]
        );
    }

    /**
     * Получить запись из таблицы users по token.
     * 
     * @param {string} token 
     * @returns {Promise}
     */
    getUserByToken(token) {
        const user = this.db.get('SELECT * FROM users WHERE token=?', [token]);
        return user;
    }

    /**
     * Добавление новой записи в таблицу tasks
     * 
     * @param {string} name 
     * @param {string} description 
     * @param {string} datetime 
     * @param {string} status 
     * @param {number} priority 
     * @returns {Promise}
     */
    addNewTask(name, description, datetime, status, priority, guid, isDeleted = 0) {
        const result = this.db.run(`
            INSERT INTO 
                tasks 
                (name, description, status, priority, datetime, guid, is_deleted, datetime_deleted) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            `, 
            [name, description, status, priority, datetime, guid, isDeleted, ""]
        );
        return result;
    }   

    /**
     * Получить id задачи
     * 
     * @param {string} name 
     * @param {string} datetime 
     * @returns {Promise}
     */
    getTaskIdByDatetimeName(name, datetime) {
        const taskId = this.db.get(`
            SELECT
                id
            FROM
                tasks
            WHERE	
                name = ?
                AND datetime = ?
        `, [name, datetime]);
        return taskId;
    }

        /**
     * Получить id задачи по guid
     * 
     * @param {string} guid 
     * @returns {Promise}
     */
    getTaskIdByGuid(guid) {
        const taskId = this.db.get(`
            SELECT
                id
            FROM
                tasks
            WHERE	
                guid = ?
        `, [guid]);
        return taskId;
    }

    /**
     * Добавление новой записи в таблицу users_tasks.
     * 
     * @param {string} userId 
     * @param {string} taskId 
     * @returns {Promise}
     */
    addRelationUserTask(userId, taskId) {
        const result = this.db.run(`
            INSERT INTO
                users_tasks
            (user_id, task_id)
            VALUES (?, ?)
        `, [userId, taskId]);
        return result;
    }

    /**
     * Обновить запись в таблице tasks.
     * 
     * @param {string} name 
     * @param {string} datetime 
     * @param {string} status 
     * @param {integer} priority 
     * @param {string} description 
     * @param {integer} taskId
     * @param {string} datetimeDeleted
     * @param {integer} isDeleted 
     * @returns {Promise}
     */
    updateTask(name, datetime, status, priority, description, taskId, datetimeDeleted, isDeleted) {
        const result = this.db.run(`
            UPDATE 
                tasks 
            SET 
                name = ?, 
                status = ?,
                priority = ?,
                description = ?,
                datetime = ?,
                datetime_deleted = ?,
                is_deleted = ?
            WHERE 
                id = ?`,
            [name, status, priority, description, datetime, datetimeDeleted, isDeleted, taskId]
        );
        return result;
    }

    /**
     * Обновить значение колонки status в таблице tasks.
     * 
     * @param {string} status 
     * @param {integer} taskId 
     * @returns {Promise}
     */
     updateTaskStatus(status, taskId) {
        const result = this.db.run(`
            UPDATE 
                tasks 
            SET 
                status = ?
            WHERE 
                id = ?`,
            [status, taskId]
        );
        return result;
    }

    /**
     * Обновить значение колонки priority в таблице tasks.
     * 
     * @param {integer} priority 
     * @param {integer} taskId 
     * @returns {Promise}
     */
     updateTaskPriority(priority, taskId) {
        const result = this.db.run(`
            UPDATE 
                tasks 
            SET 
                priority = ?
            WHERE 
                id = ?`,
            [priority, taskId]
        );
        return result;
    }

    /**
     * Возвращает запись, если существует связь между userId и taskId. 
     * 
     * @param {integer} userId 
     * @param {integer} taskId 
     * @returns {Promise}
     */
    getUserTaskRelation(userId, taskId) {
        const result = this.db.get(`
            SELECT 
                1 AS result
            FROM 
                users_tasks
            WHERE
                task_id = ?
                AND user_id = ?`,
            [taskId, userId]
        );
        return result;
    }

    /**
     * Удалить запись в таблице tasks.
     * 
     * @param {integer} taskId 
     * @returns {Promise}
     */
    removeTask(taskId) {
        const result = this.db.run(`
            DELETE FROM tasks WHERE id = ?`,
            [taskId]
        );
        return result;
    }

    /**
     * Удалить запись в таблице users_tasks.
     * 
     * @param {integer} taskId 
     * @returns 
     */
    removeUserRaskRelationByTaskId(taskId) {
        const result = this.db.run(`
            DELETE FROM users_tasks WHERE task_id = ?`,
            [taskId]
        );
        return result;
    }

    /**
     * Получить все записи таблицы tasks.
     * 
     * @returns {Promise}
     */
    getAllTasks() {
        const result = this.db.all(`
            SELECT 
                id,
                name,
                description,
                datetime,
                status,
                priority,
                guid,
                is_deleted,
                datetime_deleted
            FROM
                tasks`
        );
        return result;
    }

    /**
     * Получить список задач по userId.
     * 
     * @param {integer} userId 
     * @returns {Promise}
     */
    getTasksByUserId(userId) {
        const result = this.db.all(`
            SELECT 
                tasks.id AS id,
                tasks.name AS name,
                tasks.description AS description,
                tasks.datetime AS datetime,
                tasks.status AS status,
                tasks.priority AS priority,
                tasks.guid AS guid,
                tasks.is_deleted AS is_deleted,
                tasks.datetime_deleted AS datetime_deleted,
                tasks_files.id AS file_id,
                tasks_files.src AS file_src,
                tasks_files.file_name AS file_name,
                tasks_files.datetime AS file_datetime,
                tasks_files.task_id AS file_task_id,
                tasks_files.guid AS file_guid
            FROM
                tasks 
                INNER JOIN users_tasks 
                    ON (tasks.id = users_tasks.task_id)
                LEFT JOIN tasks_files
                    ON (tasks.id = tasks_files.task_id)
                
            WHERE 
                users_tasks.user_id = ?
                AND tasks.is_deleted = 0
            `, [userId]
        );
        return result;
    }

    /**
     * Получить список задач по userId.
     * 
     * @param {integer} userId 
     * @returns {Promise}
     */
     getTaskByTaskId(taskId) {
        const result = this.db.get(`
            SELECT 
                id,
                name,
                description,
                datetime,
                status,
                priority,
                guid,
                is_deleted,
                datetime_deleted
            FROM
                tasks
            WHERE 
                id = ?`,
            [taskId]
        );
        return result;
    }

    /**
     * Добавляет запись в таблицу tasks_files
     * 
     * @param {integer} taskId 
     * @param {string} src 
     * @param {string} filename 
     * @param {string} datetime 
     * @returns {Promise} 
     */
    saveTaskFile(taskId, src, filename, datetime, guid) {
        const result = this.db.run(`
            INSERT INTO
                tasks_files
                (src, file_name, datetime, task_id, guid)
            VALUES
                (?, ?, ?, ?, ?)
        `, [src, filename, datetime, taskId, guid]);
        return result;
    }

    /**
     * Получить запись таблицы tasks_files по guid
     * 
     * @param {string} guid 
     * @returns 
     */
    getTaskFileByGUID(guid) {
        const result = this.db.get(`
            SELECT
                id,
                src, 
                file_name, 
                datetime, 
                task_id,
                guid
            FROM 
                tasks_files
            WHERE
            guid = ?
        `, [guid]);
        return result;
    }

    /**
     * Удаляет запись в таблице tasks_files
     * 
     * @param {integer} taskFileId 
     * @returns {Promise} 
     */
    removeTaskFile(taskFileId) {
        const result = this.db.run(`
            DELETE FROM 
                tasks_files 
            WHERE 
                id = ?
        `, [taskFileId]);
        return result;
    }

    /**
     * Получить записи таблица tasks_files по task_id
     * 
     * @param {integer} taskId 
     * @returns {Promise} 
     */
    getTaskFiles(taskId) {
        const result = this.db.all(`
            SELECT
                id,
                src,
                file_name,
                datetime,
                task_id,
                guid
            FROM 
                tasks_files
            WHERE
                task_id = ?
        `, [taskId]);
        return result;
    }

    /**
     * Получить id файла по id и task_id.
     * 
     * @param {number} fileId 
     * @param {number} taskId 
     */
    getTaskFilesByTaskIdAndFileId(fileId, taskId) {
        const result = this.db.get(`
            SELECT
                *
            FROM
                tasks_files
            WHERE
                id = ?
                AND task_id = ?
            `, [fileId, taskId]
        );
        return result;
    }

    /**
     * 
     * @param {number} taskId 
     * @param {number} isDeleted 
     * @param {string} datetimeDeleted 
     * @returns 
     */
    setDeletedStatus(taskId, isDeleted, datetimeDeleted = "") {
        const result = this.db.run(`
            UPDATE 
                tasks 
            SET 
                is_deleted = ?,
                datetime_deleted = ?
            WHERE 
                id = ?
            `, [isDeleted, datetimeDeleted, taskId]
        );
        return result;
    }

    /**
     * Получить список задач помечанных на удаление по userId.
     * 
     * @param {integer} userId 
     * @returns {Promise}
     */
     getDeletedTasksByUserId(userId) {
        const result = this.db.all(`
            SELECT 
                tasks.id AS id,
                tasks.name AS name,
                tasks.description AS description,
                tasks.datetime AS datetime,
                tasks.status AS status,
                tasks.priority AS priority,
                tasks.guid AS guid,
                tasks.is_deleted AS is_deleted,
                tasks.datetime_deleted AS datetime_deleted,
                tasks_files.id AS file_id,
                tasks_files.src AS file_src,
                tasks_files.file_name AS file_name,
                tasks_files.datetime AS file_datetime,
                tasks_files.task_id AS file_task_id,
                tasks_files.guid AS file_guid
            FROM
                tasks 
                INNER JOIN users_tasks 
                    ON (tasks.id = users_tasks.task_id)
                LEFT JOIN tasks_files
                    ON (tasks.id = tasks_files.task_id)
                
            WHERE 
                users_tasks.user_id = ?
                AND tasks.is_deleted = 1
            `, [userId]
        );
        return result;
    }

}



module.exports = DB;