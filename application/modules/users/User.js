
class User {
    constructor({ id, name, email, password, token }) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
    }

    fill({ id, name, email, password, token}) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token; 
    }

    clone() {
        return new User({
            id: this.id,
            name: this.name,
            password: this.password,
            email: this.email,
            token: this.token
        });
    }

}

module.exports = User;