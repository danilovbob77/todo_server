const Module = require('../module/Module');
const Answer = require('../../routes/Answer');
const commonModule = require('../commonModule');
const { STATUSES } = require('../../../settings');
const uuid = require('uuid');
const Task = require('./Task');
const fs = require('fs');


class TasksManager extends Module {
    constructor(params) {
        super(params);
        this._statuses = STATUSES;
    }

    // Добавление новой задачи.
    async addNewTask(data) {
        const { token, name, description, datetime, status, priority } = data;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const userId = await this.db.getUserByToken(token);

        if (!userId) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        if (!name) {
            return Answer.getDataToTemplate(false, "Не задано имя задачи");
        }

        const thisDateTimeByString = commonModule.checkDate(datetime); 
        const thisDescription = (typeof description === "string") ? description : "";
        const thisStatus = (typeof status === "string" && this._valueIsInStatuses(status)) ? status : this._statuses[0];
        const thisPriority = (
            typeof priority === "number" 
            && priority >= 0 
            && priority <= 3
            && Number.isInteger(priority)) ? priority : 0;

        const guid = uuid.v4();
        const resultAddTask = await this.db.addNewTask(name, thisDescription, thisDateTimeByString, thisStatus, thisPriority, guid);

        if (!resultAddTask) {
            return Answer.getDataToTemplate(false, "Не удалось создать задачу");
        }

        const taskId = await this.db.getTaskIdByGuid(guid);

        if (!taskId) {
            return Answer.getDataToTemplate(false, "Ошибка при поиске задачи в базе данныъ");
        }

        const result = await this.db.addRelationUserTask(userId.id, taskId.id);

        if (!result) {
            return Answer.getDataToTemplate(false, "Ошибка при добавление связи между пользователем и задачей")
        }

        const taskData = await this.db.getTaskByTaskId(taskId.id);

        if (!taskData) {
            return Answer.getDataToTemplate(false, "Ошибка при запросе в базу данных")
        }

        const taskFiles = await this.db.getTaskFiles(taskId);

        if (!Array.isArray(taskFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось получить файлы по задаче")
        }

        const task = new Task({ ...taskData, files: taskFiles })

        return Answer.getDataToTemplate(task);
    }

    // Обновить задачу.
    async updateTask(data) {
        const { 
            token, 
            name, 
            description, 
            datetime, 
            status, 
            priority, 
            task_id: taskId,
            datetime_deleted,
            is_deleted
        } = data;

        const taskIdNumber = Number(taskId);

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        if (!name) {
            return Answer.getDataToTemplate(false, "Не задано имя задачи");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskIdNumber);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const thisDateTimeByString = commonModule.checkDate(datetime); 
        const thisDescription = (typeof description === "string") ? description : "";
        const thisStatus = (typeof status === "string" && this._valueIsInStatuses(status)) ? status : this._statuses[0];

        const priorityNumber = Number(priority);
        const thisPriority = (
            typeof priorityNumber === "number" 
            && priorityNumber >= 0 
            && priorityNumber <= 3
            && Number.isInteger(priorityNumber)) ? priorityNumber : 0;

        let isDeleted = false;
        if (typeof is_deleted === "boolean") {
            isDeleted = is_deleted;
        } else if (typeof is_deleted === "number") {
            isDeleted = (is_deleted === 1);   
        } else {
            isDeleted = (is_deleted === "1");
        }

        const thisDateTimeDeletedByString = isDeleted ? commonModule.checkDate(datetime_deleted) : ""; 

        const result = await this.db.updateTask(
            name, 
            thisDateTimeByString, 
            thisStatus,
            thisPriority,
            thisDescription,
            taskIdNumber,
            thisDateTimeDeletedByString,
            isDeleted ? 1 : 0
        );

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось обновить задачу");
        }

        const taskData = await this.db.getTaskByTaskId(taskIdNumber);

        if (!taskData) {
            return Answer.getDataToTemplate(false, "Ошибка при запросе в базу данных")
        }

        const taskFiles = await this.db.getTaskFiles(taskIdNumber);

        if (!Array.isArray(taskFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось получить файлы по задаче")
        }

        const task = new Task({ ...taskData, files: taskFiles })

        return Answer.getDataToTemplate(task);
    }

    // Обновить статус задачи.
    async updateTaskStatus(data) {
        const { token, status, task_id: taskId } = data;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskId);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const thisStatus = (typeof status === "string" && this._valueIsInStatuses(status)) ? status : this._statuses[0];

        const result = await this.db.updateTaskStatus(
            thisStatus, 
            user.id
        );

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось обновить статус задачи");
        }

        return Answer.getDataToTemplate(thisStatus);
    }

    // Обновить приоритет задачи.
    async updateTaskPriority(data) {
        const { token, priority, task_id: taskId } = data;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskId);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const thisPriority = (
            typeof priority === "number" 
            && priority >= 0 
            && priority <= 3
            && Number.isInteger(priority)) ? priority : 1;

        const result = await this.db.updateTaskPriority(
            thisPriority, 
            user.id
        );

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось обновить приоритет задачи");
        }

        return Answer.getDataToTemplate(thisPriority);
    }

    // Обновить приоритет задачи.
    async removeTask(data) {
        const { token, task_id: taskId } = data;

        const taskIdNumber = Number(taskId);

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskIdNumber);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const resultRemoveTaskUserRelation = await this.db.removeUserRaskRelationByTaskId(taskIdNumber);

        if (!resultRemoveTaskUserRelation) {
            return Answer.getDataToTemplate(false, "Не удалось удалить связь задачи с пользователем");
        }

        const result = await this.db.removeTask(taskIdNumber);

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось удалить задачу");
        }

        return Answer.getDataToTemplate(true);
    }

    // Получить список задач по пользователя.
    async getTasksByUser(data) {
        const { token } = data;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const usersTasksAndTasksFiles = await this.db.getTasksByUserId(user.id);

        if (!Array.isArray(usersTasksAndTasksFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось выбрать задачи");
        }

        const result = commonModule.handleTasksAndTasksFiles(usersTasksAndTasksFiles);

        return Answer.getDataToTemplate(result);
    }

    // Получить список задач по пользователя.
    async getAllTasks(data) {
        const result = await this.db.getAllTasks();

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось выбрать задачи");
        }

        return Answer.getDataToTemplate(result);
    }

    async saveTaskImage(data) {
        const { token, 
            task_image: taskImage, 
            task_id: taskId,
            datetime_save: datetime 
        } = data;

        if (!taskImage) {
            return Answer.getDataToTemplate(false, "Файл не передан с клиента на сервер");
        }

        const filename = taskImage.filename;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const userIdObject = await this.db.getUserByToken(token);

        if (!userIdObject) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(userIdObject.id, taskId);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const datetimeByString = commonModule.checkDate(datetime); 
        const src = commonModule.getPathToFileAPI(filename);

        const guid = uuid.v4();

        const result = await this.db.saveTaskFile(taskId, src, filename, datetimeByString, guid);

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось сохранить файл");
        }

        const fileDate = await this.db.getTaskFileByGUID(guid);

        if (!fileDate) {
            return Answer.getDataToTemplate(false, "Не удалось получить информацию о файле");
        }

        return Answer.getDataToTemplate(fileDate);
    }

    async removeTaskFile(data) {
        const { 
            token, 
            task_id: taskId,
            file_id: fileId,
        } = data;

        const fileIdNumber = Number(fileId);
        const taskIdNumber = Number(taskId);

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в базе не найден");
        }

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskIdNumber);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const taskFileObj = await this.db.getTaskFilesByTaskIdAndFileId(fileIdNumber, taskIdNumber);

        if (!taskFileObj) {
            return Answer.getDataToTemplate(false, "Файл и задача не соответствуют");
        }

        let resultRemoveFileFromFolder = undefined;
        try {
            fs.unlinkSync(commonModule.getPathToFileInDateisystem(taskFileObj.file_name));
            resultRemoveFileFromFolder = true;
        } catch (err) {
            console.error(err);
        }

        if (!resultRemoveFileFromFolder) {
            return Answer.getDataToTemplate(false, "Не удалось удалить файл из директории хранения");
        }

        const result = await this.db.removeTaskFile(taskFileObj.id);

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось удалить запись файл задачи БД");
        }

        return Answer.getDataToTemplate(true);
    }

    async setTaskIsDeleted(data) {
        const { 
            token, 
            task_id: taskId,
            datetime_deleted: datetimeDeleted
        } = data;

        const taskIdNumber = Number(taskId);

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в базе не найден");
        }   

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskIdNumber);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const thisDateTimeByString = commonModule.checkDate(datetimeDeleted); 

        const result = await this.db.setDeletedStatus(taskId, 1, thisDateTimeByString);

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось задачу пометить на удаление");
        }

        const taskData = await this.db.getTaskByTaskId(taskId);

        if (!taskData) {
            return Answer.getDataToTemplate(false, "Ошибка при запросе в базу данных")
        }

        const taskFiles = await this.db.getTaskFiles(taskId);

        if (!Array.isArray(taskFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось получить файлы по задаче")
        }

        const task = new Task({ ...taskData, files: taskFiles })

        return Answer.getDataToTemplate(task);
    }

    async setTaskIsNotDeleted(data) {
        const { 
            token, 
            task_id: taskId
        } = data;

        const taskIdNumber = Number(taskId);

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в базе не найден");
        }   

        const resultUserTaskRelation = await this.db.getUserTaskRelation(user.id, taskIdNumber);

        if (!resultUserTaskRelation) {
            return Answer.getDataToTemplate(false, "Данная задача не соовтетствуют текущему пользователю");
        }

        const result = await this.db.setDeletedStatus(taskId, 0);

        if (!result) {
            return Answer.getDataToTemplate(false, "Не удалось с задачи снять пометку");
        }

        const taskData = await this.db.getTaskByTaskId(taskId);

        if (!taskData) {
            return Answer.getDataToTemplate(false, "Ошибка при запросе в базу данных")
        }

        const taskFiles = await this.db.getTaskFiles(taskId);

        if (!Array.isArray(taskFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось получить файлы по задаче")
        }

        const task = new Task({ ...taskData, files: taskFiles })

        return Answer.getDataToTemplate(task);
    }

    // Получить список задач по пользователя помеченных на удаление.
    async getDeletedTasksByUser(data) {
        const { token } = data;

        if (!token) {
            return Answer.getDataToTemplate(false, "Токен пользователя пустой");
        }

        const user = await this.db.getUserByToken(token);

        if (!user) {
            return Answer.getDataToTemplate(false, "Пользователь в системе не найден по токену");
        }

        const usersTasksAndTasksFiles = await this.db.getDeletedTasksByUserId(user.id);

        if (!Array.isArray(usersTasksAndTasksFiles)) {
            return Answer.getDataToTemplate(false, "Не удалось выбрать задачи");
        }

        const result = commonModule.handleTasksAndTasksFiles(usersTasksAndTasksFiles);

        return Answer.getDataToTemplate(result);
    }

    // Проверяет, что в статусах имеется значение = value.
    _valueIsInStatuses(value) {
        const result = this._statuses.find(element => element === value);
        return result !== undefined;
    }
}

module.exports = TasksManager;