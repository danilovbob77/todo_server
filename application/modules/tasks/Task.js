const TDLFile = require('../files/TDLFile');


class Task {
    constructor({ id, name, description, datetime, status, priority, guid, files, is_deleted, datetime_deleted }) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.datetime = datetime;
        this.status = status;
        this.priority = priority;
        this.guid = guid;
        this.is_deleted = (is_deleted != 0 || is_deleted != false || is_deleted != "0");
        this.datetime_deleted = datetime_deleted;
        this.files = Array.isArray(files) ? files : (files ? [files] : []);
    }

    addNewFile(file) {
        if (file instanceof TDLFile) {
            this.files.push(file);
        } else {
            const newFile = new TDLFile({ ...file });
            this.files.push(newFile);
        }
    }
}


module.exports = Task;