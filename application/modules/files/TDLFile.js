
class TDLFile {
    constructor({ id, src, datetime, file_name, task_id, guid }) {
        this.id = id;
        this.src = src;
        this.datetime = datetime;
        this.file_name = file_name;
        this.task_id = task_id;
        this.guid = guid;
    }
}


module.exports = TDLFile;