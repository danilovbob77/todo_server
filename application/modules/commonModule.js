const md5 = require('md5');
const { HOST, UPLOADS, PATH_TO_DIR } = require('../../settings');
const Task = require('./tasks/Task');
const TDLFile = require('./files/TDLFile');

module.exports.hashUserData = (email, password) => {
    const num = Math.round(Math.random() * 1000000);
    const passHash = md5(email + password);
    const token = md5(passHash + String(num));
    return { passHash, token };
}

module.exports.getCurrentDateTimeByString = (dateElement = undefined) => {
    const currentdate = (dateElement instanceof Date) ? dateElement : new Date();

    const date = currentdate.getDate();
    const month = currentdate.getMonth()+1;
    const hours = currentdate.getHours();
    const minutes = currentdate.getMinutes();
    const seconds = currentdate.getSeconds();

    const dateString = addLeadingZero(date) + "."
    + addLeadingZero(month) + "." 
    + currentdate.getFullYear() + " "  
    + addLeadingZero(hours) + ":"  
    + addLeadingZero(minutes) + ":" 
    + addLeadingZero(seconds);

    return dateString;
}

module.exports.checkDate = (date) => {
    let result = null;
    if (typeof date === "string") {
        const dateTimeRegax = /\d\d.\d\d.\d\d\d\d \d\d:\d\d:\d\d/;
        if (date && dateTimeRegax.test(date)) {
            result = date;
        } else {
            result = this.getCurrentDateTimeByString();
        }
    } else if (typeof date === "object") {
        if (date instanceof Date) {
            result = this.getCurrentDateTimeByString(date);
        } else {
            result = this.getCurrentDateTimeByString();
        }
    } else {
        result = this.getCurrentDateTimeByString();
    }
    return result;
}

module.exports.getPathToFileAPI = (filename) => {
    return filename ? `${HOST}/${UPLOADS.IMAGES.TODO.FOLDER_PATH}/${filename}` : '';
}

module.exports.getPathToFileInDateisystem = (filename) => {
    return filename ? `${PATH_TO_DIR}/${UPLOADS.IMAGES.TODO.FOLDER_PATH}/${filename}` : '';
}

module.exports.handleTasksAndTasksFiles = (arrayData) => {
    const result = []; 
    arrayData.forEach(currentElement => {
        const elementInResultArray = result.find(element => element.id === currentElement.id);
        if (elementInResultArray !== undefined) {
            if (currentElement.file_id !== null) {
                const newFile = new TDLFile({
                    id: currentElement.file_id,
                    src: currentElement.file_src,
                    file_name: currentElement.file_name,
                    datetime: currentElement.file_datetime,
                    task_id: currentElement.file_task_id,
                    guid: currentElement.file_guid
                });
                elementInResultArray.addNewFile(newFile);
            }
        } else {
            const newTask = new Task({
                id: currentElement.id,
                name: currentElement.name,
                description: currentElement.description,
                datetime: currentElement.datetime,
                status: currentElement.status,
                priority: currentElement.priority,
                guid: currentElement.guid,
                is_deleted: currentElement.is_deleted,
                datetime_deleted: currentElement.datetime_deleted,
                files: []
            });
            if (currentElement.file_id !== null) {
                const newFile = new TDLFile({
                    id: currentElement.file_id,
                    src: currentElement.file_src,
                    file_name: currentElement.file_name,
                    datetime: currentElement.file_datetime,
                    task_id: currentElement.file_task_id,
                    guid: currentElement.file_guid
                });
                newTask.addNewFile(newFile);
            }
            result.push(newTask);
        }
    });
    return result;
}

const findTasksAndTasksFiles = ({ id, name, description, datetime, status, priority, guid }) => {

}

const addLeadingZero = (value) => {
    return value >= 10 ? "" + value : "0" + value;
}