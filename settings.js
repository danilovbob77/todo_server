const path = require('path');


const SETTINGS = {
    HOST: 'http://localhost:8080',
    PORT: 8080,
    PATH_TO_DIR: path.dirname(__filename), 
    DB_PATH: './application/modules/DB/db.db',
    STATUSES: [
        "Не выбрана",
        "Запланирована",
        "В работе",
        "Выполнена"
    ],
    UPLOADS: {
        FOLDER_PATH: "uploads",
        IMAGES: {
            FOLDER_PATH: "uploads/images",
            TODO: {
                FOLDER_PATH: "uploads/images/todo"
            },
        },
    },
    PATH_TO_DIR: path.dirname(__filename), 
    MULTER: {
        TASK_PICTURE: {
            NAME: "task_image"
        }
    }
};

module.exports = SETTINGS;